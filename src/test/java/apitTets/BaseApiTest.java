package apitTets;


import com.jayway.restassured.RestAssured;
import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.specification.*;
import org.testng.annotations.BeforeClass;


import static utils.ConstData.*;

public class BaseApiTest {
    protected static RequestSpecification petStoreRequestSpecification = null;

    @BeforeClass
    public static void beforeTests(){
        RestAssured.baseURI = BASE_URL;
        RestAssured.basePath = BASE_PATH;

        petStoreRequestSpecification = new RequestSpecBuilder()
                .setContentType(ContentType.JSON)
                .setAccept(ContentType.JSON)
                .build();
        RestAssured.requestSpecification = petStoreRequestSpecification;
    }
}
