package apitTets;

import dataobjects.User;
import dataobjects.UserResponse;


import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import requests.UserRequests;



public class UserApiTests extends BaseApiTest{

    public static UserRequests requests = null;
    public static User user = null;
    public static User[] users = new User[3];


    @BeforeClass
    public static void beforeUserApiTest(){
        requests = new UserRequests();
        user = new User.UserBuilder()
                .withId(12L)
                .withUsername("Student")
                .withFirstName("Ivan")
                .withLastName("Ivanov")
                .withEmail("ivanov@epam.com")
                .withPassword("1234")
                .withPhone("38098746682")
                .withUserStatus(true)
                .build();
        for(int i=0; i<3; i++){
            users[i] = new User.UserBuilder()
                    .withId(Long.parseLong(""+(i+2)))
                    .withUsername("user"+i)
                    .withFirstName("name"+i)
                    .withLastName("lastName"+i)
                    .withEmail("email"+i)
                    .withPassword(""+i)
                    .withPhone(""+i)
                    .withUserStatus(true)
                    .build();
        }
    }
    @Test(priority = 1)
    public void canCreateUser() {
        UserResponse expectedResult = new UserResponse(200L, "unknown", "12");
        UserResponse actualResult = requests.createUser(user);
        User createdUser = requests.getUserByUsername(user.getUsername());
        Assert.assertEquals(actualResult,expectedResult);
        Assert.assertEquals(createdUser,user);
    }

    @Test(priority = 2)
    public void getCreatedUser(){
        User createdUser = requests.getUserByUsername(user.getUsername());
        Assert.assertEquals(createdUser,user);
    }

    @Test(priority = 3)
    public void canLoginAsUser(){
        UserResponse expectedResult = new UserResponse(200L, "unknown", "logged in user");
        UserResponse actualResult = requests.loginAsUser(user.getUsername(), user.getPassword());
        Assert.assertEquals(actualResult.getCode(),expectedResult.getCode());
        Assert.assertEquals(actualResult.getType(),expectedResult.getType());
        Assert.assertTrue(actualResult.getMessage().contains(expectedResult.getMessage()));
    }

    @Test(priority = 4)
    public void canLogout(){
        UserResponse expectedResult = new UserResponse(200L, "unknown", "ok");
        UserResponse actualResult = requests.logoutAsUser();
        Assert.assertEquals(actualResult,expectedResult);
    }

    @Test(priority = 5)
    public void createUsersWithArray(){
        UserResponse expectedResult = new UserResponse(200L, "unknown", "ok");
        UserResponse actualResult = requests.addUsersWithArray(users);
        Assert.assertEquals(actualResult,expectedResult);
    }

    @Test(priority = 6)
    public void createUsersWithList(){
        UserResponse expectedResult = new UserResponse(200L, "unknown", "ok");
        UserResponse actualResult = requests.addUsersWithList(users);
        Assert.assertEquals(actualResult,expectedResult);
    }
    @Test(priority = 7)
    public void canDeleteUser(){
        UserResponse expectedResult = new UserResponse(200L, "unknown", "Student");
        UserResponse actualResult = requests.deleteUser(user.getUsername());
        Assert.assertEquals(actualResult,expectedResult);
    }
}
