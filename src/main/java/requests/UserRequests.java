package requests;

import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import dataobjects.User;
import dataobjects.UserResponse;

import static utils.ConstData.*;


public class UserRequests extends HTTPRequests {

    public UserResponse createUser(User user) {
        Response response = doPost(USER, user);
        response.then().statusCode(200);
        response.then().contentType(ContentType.JSON);
        return response.getBody().as(UserResponse.class);
    }

    public User getUserByUsername(String userName) {
        return  doGet(USER + "/" + userName).as(User.class);
    }

    public UserResponse loginAsUser(String userName, String password){
        Response response = doGet(USER_LOGIN, userName, password);
        response.then().statusCode(200);
        response.then().contentType(ContentType.JSON);
        return response.getBody().as(UserResponse.class);
    }

    public UserResponse logoutAsUser(){
        Response response = doGet(USER_LOGOUT);
        response.then().statusCode(200);
        response.then().contentType(ContentType.JSON);
        return response.getBody().as(UserResponse.class);
    }

    public UserResponse deleteUser(String userName){
        Response response = doDelete(USER+"/"+userName);
        response.then().statusCode(200);
        response.then().contentType(ContentType.JSON);
        return response.getBody().as(UserResponse.class);
    }

    public UserResponse addUsersWithArray(User[] users){
        Response response = doPost(ADD_USER_ARRAY, users);
        response.then().statusCode(200);
        response.then().contentType(ContentType.JSON);
        return response.getBody().as(UserResponse.class);
    }
    public UserResponse addUsersWithList(User[] users){
        Response response = doPost(ADD_USER_LIST, users);
        response.then().statusCode(200);
        response.then().contentType(ContentType.JSON);
        return response.getBody().as(UserResponse.class);
    }

}
