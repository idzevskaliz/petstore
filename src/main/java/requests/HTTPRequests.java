package requests;


import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;

public class HTTPRequests {

    protected Response doGet(String url){
        return RestAssured.given().when().get(url);
    }
    protected Response doGet(String url, String param, String password){
        return RestAssured.given().queryParam(param,password).when().get(url);
    }
    protected Response doPost(String url, Object body){
        return RestAssured.given().body(body).when().post(url);
    }

    protected Response doPut(String url, Object body){
        return RestAssured.given().body(body).when().post(url);
    }

    protected Response doDelete(String url){
        return RestAssured.given().when().delete(url);
    }

}
