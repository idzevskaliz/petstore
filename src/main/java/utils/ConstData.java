package utils;

public final class ConstData {

    public static final String BASE_URL = "https://petstore.swagger.io";
    public static final String BASE_PATH = "/v2";
    public static final String USER = "/user";
    public static final String USER_LOGIN = USER + "/login";
    public static final String USER_LOGOUT = USER + "/logout";
    public static final String ADD_USER_ARRAY = USER + "/createWithArray";
    public static final String ADD_USER_LIST = USER + "/createWithList";

    private ConstData(){}
}
