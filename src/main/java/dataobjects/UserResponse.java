package dataobjects;

public class UserResponse {
    private Long code;
    private String type;
    private String message;

    public UserResponse(){}

    public UserResponse(Long code, String type, String message){
        this.code = code;
        this.type = type;
        this.message = message;
    }

    public Long getCode() { return code;}

    public void setCode(Long code) { this.code = code; }

    public String getType() { return type; }

    public void setType(String type) { this.type = type; }

    public String getMessage() { return message; }

    public void setMessage(String message) { this.message = message; }

    @Override
    public int hashCode() { return super.hashCode(); }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(obj == null || getClass() != obj.getClass()) return false;

        UserResponse response = (UserResponse) obj;
        return code.equals(response.code)&&
                type.equals(response.type)&&
                message.equals(response.message);
    }

    @Override
    public String toString() {
        return "UserResponse{" +
                "code=" + code +
                ", type='" + type + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
